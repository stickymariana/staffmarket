
package model;

import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Connection;


/**
 *
 * @author Stick&Maria
 */
public class ConexionDAO {
    
    private static Connection conn;
    String url = "jdbc:oracle:thin:@localhost:1521:XE";
    String user = "PRUEBA";
    String pass = "PRUEBA";
    
    private ConexionDAO() throws SQLException {
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        conn = DriverManager.getConnection(url, user, pass);
    }
    
    public static Connection getConnection() throws SQLException{
        if (conn == null){
            new ConexionDAO();
        }
        return conn;
    }
}
