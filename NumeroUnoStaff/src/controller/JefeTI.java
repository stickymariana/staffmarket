
package controller;

/**
 *
 * @author Stick&Maria
 */
public class JefeTI extends Empleado {
    int codigo = 1;

    public JefeTI() {
    }

    public JefeTI(String rut, String nombres, String apellidoPat, String apellidoMat, int telefono) {
        super(rut, nombres, apellidoPat, apellidoMat, telefono);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "JefeTI" + "\nCodigo" + codigo;
    }
    
    
    
    
}
