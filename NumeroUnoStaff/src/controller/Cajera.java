
package controller;

/**
 *
 * @author Stick&Maria
 */
public class Cajera extends Empleado{
    int codigo = 3;

    public Cajera() {
    }

    public Cajera(String rut, String nombres, String apellidoPat, String apellidoMat, int telefono) {
        super(rut, nombres, apellidoPat, apellidoMat, telefono);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Cajera" + "\nCodigo " + codigo ;
    }
    
    
    
}
