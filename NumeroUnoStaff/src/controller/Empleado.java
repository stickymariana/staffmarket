
package controller;

/**
 *
 * @author Stick&Maria
 */
public class Empleado {
    private String rut, nombres, apellidoPat, apellidoMat;
    private int telefono;

    public Empleado() {
    }

    public Empleado(String rut, String nombres, String apellidoPat, String apellidoMat, int telefono) {
        this.rut = rut;
        this.nombres = nombres;
        this.apellidoPat = apellidoPat;
        this.apellidoMat = apellidoMat;
        this.telefono = telefono;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPat() {
        return apellidoPat;
    }

    public void setApellidoPat(String apellidoPat) {
        this.apellidoPat = apellidoPat;
    }

    public String getApellidoMat() {
        return apellidoMat;
    }

    public void setApellidoMat(String apellidoMat) {
        this.apellidoMat = apellidoMat;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Empleado" + "\nRut: " + rut + ",\\Nombres: " + nombres + "\nApellido Paterno: " + apellidoPat + 
                "\nApellido Materno: " + apellidoMat + "\nTelefono: " + telefono ;
    }
    
    
}
