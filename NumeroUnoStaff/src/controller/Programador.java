
package controller;

/**
 *
 * @author Stick&Maria
 */
public class Programador extends Empleado{
    int codigo = 2;

    public Programador() {
    }

    public Programador(String rut, String nombres, String apellidoPat, String apellidoMat, int telefono) {
        super(rut, nombres, apellidoPat, apellidoMat, telefono);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Programador" + "\nCodigo " + codigo ;
    }
   
    
    
    
    
}
