
package controller;

/**
 *
 * @author Stick&Maria
 */
public class Supervisor extends Empleado {
    int codigo = 4;

    public Supervisor() {
    }

    public Supervisor(String rut, String nombres, String apellidoPat, String apellidoMat, int telefono) {
        super(rut, nombres, apellidoPat, apellidoMat, telefono);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Supervisor" + "\nCodigo " + codigo;
    }
    
    
    
}
